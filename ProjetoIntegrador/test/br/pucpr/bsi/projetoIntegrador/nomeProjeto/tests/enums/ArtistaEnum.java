package br.pucpr.bsi.projetoIntegrador.nomeProjeto.tests.enums;

import java.util.Random;

public enum ArtistaEnum{
	
	FRANCIS_BACON("Francis Bacon"),
	LEON_BAKST("Leon Bakst"),
	JOHN_BALDESSARI("John Baldessari"), 
	HANS_BALDUNG("Hans Baldung"),
	MIROSLAW_BALKA("Miroslaw Balka"), 
	GIACOMO_BALLA("Giacomo Balla"),
	BALTHUS("Balthus");
	
	private String nome;
	
	private ArtistaEnum(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public static ArtistaEnum getArtista(){
		Random generator = new Random();
		ArtistaEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
}
